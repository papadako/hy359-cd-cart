/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 * FORTH-ICS
 *
 * @author Panagiotis Papadakos <papadako at ics.forth.gr>
 */
public class CD {

    private String description;
    private float price;
    private int id;

    public CD(String desc, float price, int id) {
        description = desc;
        this.price = price;
        this.id = id;
    }

    public void CD() {
        description = "";
        this.price = 0.0f;
        this.id = -1;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }
}
